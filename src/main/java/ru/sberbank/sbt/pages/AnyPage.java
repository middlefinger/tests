package ru.sberbank.sbt.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sberbank.sbt.lib.Init;

/**
 * Created by jake on 20.05.2016.
 */
public abstract class AnyPage {
    public AnyPage() {
        PageFactory.initElements(Init.getDriver(), this);

    }
    public static void waitPageToLoad() {
        // браузер вернул страницу. JS статус complete
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
    }
    public void click(WebElement element) {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions.elementToBeClickable(element));
        System.out.println("click to element "+element.getText());
        element.click();
    }
    public void click(By by) {
        WebElement element = Init.getDriver().findElement(by);
        click(element);
    }
    public void assertEquals(String string, WebElement element){

    }
    public void fillField(){

    }
    public void setText(){//через вебэлемент и через бай, для селекта

    }
    // селект для ненастоящего селект. для настоящих. чекбоксы
    // джавадок
// гит

}
