package ru.sberbank.sbt.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sberbank.sbt.lib.Init;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.sberbank.sbt.lib.Init.getStash;

/**
 * Created by jake on 24.05.2016.
 */
public class ConvertPage extends AnyPage{
    @FindBy(className = "currency-converter-date")
    private WebElement date;
    @FindBy(xpath = "//label[contains(@for, 'from')]")
    private WebElement changeFrom;
    @FindBy(xpath = "//label[contains(@for, 'to') and string-length(@for)=2]")
    private WebElement changeTo;
    @FindBy(id = "from")
    private WebElement from;
    @FindBy(id = "to")
    private WebElement to;
    @FindBy(className = "currency-converter-result")
    private WebElement result;
    public static boolean checkWithRegExp(String string){
        Pattern p = Pattern.compile("1\\s(RUB)|(EUR)|(USD)\\s=\\s\\d\\.\\d{4}\\s(RUB)|(EUR)|(USD)");
        Matcher m = p.matcher(string);
        return m.find();
    }
    public void openPerson() throws InterruptedException {
        Init.getDriver().get(getStash().get("urlPerson").toString());
        waitPageToLoad();
//        Проверяем заголовок
        Assert.assertEquals("«Сбербанк» - Частным клиентам", Init.getDriver().getTitle());
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.className("currency-converter-date")));
    }
    public void checkDate() {
        LocalDate today = LocalDate.now();
        Assert.assertEquals(today.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")), date.getText());
    }
    public void checkComponents() {

        Assert.assertEquals(changeFrom.getText(), "Поменять");
        Assert.assertEquals(changeTo.getText(), "На");
        Assert.assertEquals(from.getTagName(), "input");
        Assert.assertEquals(to.getTagName(), "input");
        System.out.println(result.getText());
        Assert.assertTrue("regexp", checkWithRegExp(result.getText()));
    }




}
