package ru.sberbank.sbt.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sberbank.sbt.lib.Init;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static ru.sberbank.sbt.lib.Init.getStash;

/**
 * Created by jake on 20.05.2016.
 */
public class InsuranceTravelersPage extends AnyPage{
    @FindBy(name="duration")
    private WebElement duration;
    @FindBy(xpath="//h2[text()='Страхование путешественников']")
    private WebElement title;
    @FindBy(css=".ng-binding.ng-scope.b-dropdown-title")
    private WebElement region;
    @FindBy(name="startDate")
    private WebElement sinceDate;
    @FindBy(name="finishDate")
    private WebElement dateUntil;
    @FindBy(name="insuredCount60")
    private  WebElement adult;
    @FindBy(name="insuredCount2")
    private WebElement young;
    @FindBy(name="insuredCount70")
    private WebElement old;
    @FindBy(className="b-checkbox-field-entity")
    private WebElement allYear;
    @FindBy(xpath = "//div[contains(@class, 'box-title') and contains(text(), 'Минимальная')]")
    private WebElement minimum;
    @FindBy(xpath = "//div[contains(@class, 'box-title') and contains(text(), 'Достаточная')]")
    private WebElement enough;
    @FindBy(xpath = "//div[contains(@class, 'box-title') and contains(text(), 'Минимальная')]/../span[contains(@class, 'b-checked-checkbox-field')]")
    private WebElement checkMin;
    @FindBy(xpath = "//div[contains(@class, 'box-title') and contains(text(), 'Минимальная')]/..")
    private WebElement minBox;
    @FindBy(css = ".ng-scope.b-form-dop-pack-box")
    private List<WebElement> recommended;
    @FindBy(xpath = ".//section/div/div/span[contains(@ng-class, 'b-form-prog-box-check-pos b-checked-checkbox-field')]")
    private List<WebElement> checkbox;
    @FindBy(className = "b-heading-tabset-img-active-2")
    private WebElement tab2;
    @FindBy(className = "b-heading-tabset-img-active-3")
    private WebElement tab3;
    @FindBy(xpath = ".//span[contains(@class, 'b-form-box-title') and contains(text(), 'Спортивный')]")
    private WebElement sport;
    @FindBy(xpath=".//span[contains(@class, 'b-form-box-title') and contains(text(), 'Предусмотрительный')]")
    private WebElement noRisk;
    @FindBy(xpath = ".//span[contains(@class, 'b-form-box-title') and contains(text(), 'Защита багажа')]")
    private WebElement protect;

    public InsuranceTravelersPage() {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//h2[text()='Страхование путешественников']")));
    }
    private WebElement getSum(WebDriver driver) throws InterruptedException {
        Thread.sleep(10000);
        return driver.findElement(By.xpath(".//dd[contains(@class, 'b-form-big-font-size')]/span"));
    }
    public void checkDefault() throws InterruptedException {
//        Регион
        Assert.assertEquals("Весь мир, кроме США и РФ", region.getText());
//        дата начала
        LocalDate today = LocalDate.now();
        LocalDate since = today.plusDays(5);
        LocalDate until = since.plusWeeks(2);
        Assert.assertEquals(since.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), sinceDate.getAttribute("value"));
//        конца
        Assert.assertEquals(until.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), dateUntil.getAttribute("value"));
//      Срок действия
        Assert.assertEquals("15", duration.getAttribute("value"));
//      Застрахованные взрослые
        Assert.assertEquals("1", adult.getAttribute("value"));
//      дети
        Assert.assertEquals("0", young.getAttribute("value"));
//      старики
        Assert.assertEquals("0", old.getAttribute("value"));
//      " Хочу, чтобы полис действовал весь год"
        Assert.assertFalse(allYear.getAttribute("checked"), false);
//      Кликнуть "минимальная"
        click(minimum);
//         Снять выделение в блоке рекоммендованного
        click(protect);
//      галочка
        new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(By
                .xpath("//div[contains(@class, 'box-title') and contains(text(), 'Минимальная')]/../span[contains(@class, 'b-checked-checkbox-field')]")));
//      оранжевая рамочка
        Assert.assertEquals("rgba(255, 167, 21, 1)", minBox.getCssValue("border-bottom-color"));
        Assert.assertEquals("rgba(255, 167, 21, 1)", minBox.getCssValue("border-left-color"));
        Assert.assertEquals("rgba(255, 167, 21, 1)", minBox.getCssValue("border-right-color"));
        Assert.assertEquals("rgba(255, 167, 21, 1)", minBox.getCssValue("border-top-color"));
//        Проверка всех кнопок в блоке рекоммендации
        for (WebElement box : recommended) {
            Assert.assertFalse(box.getAttribute("class").contains("b-form-active-box"));
            Assert.assertEquals("rgba(235, 237, 236, 1)", box.getCssValue("border-top-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)", box.getCssValue("border-bottom-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)", box.getCssValue("border-left-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)", box.getCssValue("border-right-color"));
        }
        for (WebElement greenCheck : checkbox) {
            Assert.assertEquals("", greenCheck.getAttribute("class"));
        }
    }
    public void enable() {
//      Проверить доступность вкладок «2 Оформление», «3 Подтверждение»
            Assert.assertFalse((tab2.isDisplayed()&&tab2.isEnabled()));
            Assert.assertFalse((tab3.isDisplayed()&&tab2.isEnabled()));
    }
    public void checkSum() throws InterruptedException {
        //      проверка суммы
        Assert.assertTrue("sum", getSum(Init.getDriver()).getText().contains(getStash().get("sumMin").toString()));
    }
    public void enough() {
        //      "достаточная"
        click(enough);
        enough.findElement(By.xpath("../span")).isDisplayed();
    }
    public void checkSum1() throws InterruptedException {
        //        Проверить значение «Итоговая стоимость» в разделе «Стоимость и срок действия».
        Assert.assertTrue(getSum(Init.getDriver()).getText().contains(getStash().get("sumEnough").toString()));
    }
    public void sport() throws InterruptedException {
        //    В секции «Рекомендуем предусмотреть» выбрать «Спортивный»
//  и проверить значение «Итоговая стоимость» в разделе «Стоимость и срок действия».
        click(sport);
        Assert.assertTrue(getSum(Init.getDriver()).getText().contains(getStash().get("sumSport").toString()));
    }
    public void sportText() {
//        Проверить текст значения «Спортивный» в блоке «Рекомендуем предусмотреть»
        Assert.assertTrue("sport", sport.getText().contains("Спортивный" +
                "Активные виды спорта" +
                "Защита спортинвентаря" +
                "Ski-pass / Лавина" +
                getStash().get("sportCount").toString()));
        /*Assert.assertEquals(sport.getText(), "Спортивный\n" +
                "\n" +
                "    Активные виды спорта\n" +
                "    Защита спортинвентаря\n" +
                "    Ski-pass / Лавина " +
                getStash().get("sportCount").toString());*/
    }
    public void noRisk() throws InterruptedException {
        //        В секции «Рекомендуем предусмотреть» выбрать дополнительно «Предусмотрительный»
        // и проверить значение «Итоговая стоимость» в разделе «Стоимость и срок действия».
        click(noRisk);
        Assert.assertTrue("risk",getSum(Init.getDriver()).getText().contains(getStash().get("sumNoRisk").toString()));
    }
    public void checkSum2() throws InterruptedException {
        //        В секции «Рекомендуем предусмотреть» выбрать дополнительно «Защита багажа» ,
// отключить значение «Спортивный» и
// проверить значение «Итоговая стоимость» в разделе «Стоимость и срок действия».
        click(protect);
        click(sport);
        Assert.assertTrue("final",getSum(Init.getDriver()).getText().contains(getStash().get("sumProtectNoSport").toString()));
    }
}
