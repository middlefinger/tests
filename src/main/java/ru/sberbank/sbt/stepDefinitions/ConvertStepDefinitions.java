package ru.sberbank.sbt.stepDefinitions;

import ru.sberbank.sbt.pages.ConvertPage;

/**
 * Created by jake on 24.05.2016.
 */
public class ConvertStepDefinitions {
    public void test() throws InterruptedException {
        ConvertPage page = new ConvertPage();
        page.openPerson();
        page.checkDate();
        page.checkComponents();
    }
}
