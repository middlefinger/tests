package ru.sberbank.sbt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.sberbank.sbt.stepDefinitions.ConvertStepDefinitions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static ru.sberbank.sbt.lib.Init.clearStash;
import static ru.sberbank.sbt.lib.Init.getDriver;
import static ru.sberbank.sbt.lib.Init.setStash;

/**
 * Created by jake on 24.05.2016.
 */
public class ConvertTest {
    @Test
    public void test() throws InterruptedException {
        ConvertStepDefinitions test = new ConvertStepDefinitions();
        test.test();
    }

    @Before
    public void before() throws IOException {
        Properties property = new Properties();
        property.load(new FileInputStream("src/test/java/config/application.properties"));
        setStash("browser", property.getProperty("browser"));
        setStash("webdriver.chrome.driver", property.getProperty("webdriver.chrome.driver"));
        setStash("webdriver.ie.driver", property.getProperty("webdriver.ie.driver"));
        setStash("urlPerson", property.getProperty("urlPerson"));
    }
    @After
    public void exit() {
        if (null != getDriver()){
            getDriver().quit();
        }
        clearStash();
    }
}
